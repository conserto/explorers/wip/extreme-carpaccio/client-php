FROM gitpod/workspace-full

### General Settings ###
ENV APACHE_DOCROOT="public_html"

# - install Apache
# - install PHP
USER root
RUN apt-get update \
    && chown -R gitpod:gitpod /var/run/apache2 /var/lock/apache2 /var/log/apache2 \
    && echo "include /workspace/client-php/conf/apache.conf" > /etc/apache2/apache2.conf \
    && echo ". /workspace/client-php/conf/apache.env.sh" > /etc/apache2/envvars
    
USER gitpod