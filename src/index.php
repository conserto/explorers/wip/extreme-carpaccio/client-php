<?php

include "xcarpaccio/Result.php";
include "xcarpaccio/Order.php";
include "xcarpaccio/ReaderInterface.php";
include "xcarpaccio/ResponseInterface.php";
include "xcarpaccio/FeedbackMapperInterface.php";
include "xcarpaccio/FeedbackMessage.php";
include "xcarpaccio/JsonFeedbackMapper.php";

include "xcarpaccio/Reader.php";
include "xcarpaccio/Response.php";

include "xcarpaccio/OutputInterface.php";
include "xcarpaccio/Console.php";
include "xcarpaccio/Server.php";

$reader = new Reader();
$response = new Response();
$output = new Console();
$feedbackMapper = new JsonFeedbackMapper();

$server = new Server($reader, $response, $output, $feedbackMapper);
$server->Start();